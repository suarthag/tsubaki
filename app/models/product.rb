class Product < ApplicationRecord
  validates(:name, presence: true)
  validates(:price, presence: true, numericality: { less_than: 100000})
  validates(:manufacturer, presence: true)


end
