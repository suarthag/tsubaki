class AddManufacturerAndReleased < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :manufacturer, :string, :null => false, :default => 'example'
    add_column :products, :released, :boolean, :default => false
  end
end
