Rails.application.routes.draw do
  root 'products#index'
  # get 'products/new'
  # get 'products/:id', to: 'products#show', as: 'product'
  resources :products
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
